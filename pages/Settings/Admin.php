<?php

/**
* ownCloud
*
* @author Aubrey Hewes
* @copyright 2014 Aubrey Hewes aubrey@hewes.org.uk
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU AFFERO GENERAL PUBLIC LICENSE
* License as published by the Free Software Foundation; either
* version 3 of the License, or any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU AFFERO GENERAL PUBLIC LICENSE for more details.
*
* You should have received a copy of the GNU Affero General Public
* License along with this library.  If not, see <http://www.gnu.org/licenses/>.
*/

OC_Util::checkAdminUser();

//\OCP\Util::addScript('extendedtheming', 'bootstrap');
//\OCP\Util::addStyle('extendedtheming', 'bootstrap');

OCP\Util::addScript('extendedtheming', 'settings');
OCP\Util::addStyle('extendedtheming', 'settings');

$repository = new OCA\ExtendedTheming\Repository\Theme();

$defaults = new OC_Theme(); // is now always available...

$tmpl = new OCP\Template('extendedtheming', 'Settings/Admin');
$tmpl->assign('theme', OCP\Config::getAppValue('extendedtheming', 'theme', \OC_Util::getTheme()));
$tmpl->assign('themes', $repository->findAll());
$tmpl->assign('allow_personal_theme', OCP\Config::getAppValue('extendedtheming', 'allow_personal_theme', false));
$tmpl->assign('allow_custom_theme_js', OCP\Config::getAppValue('extendedtheming', 'allow_custom_theme_js', false));
$tmpl->assign('instance_title', $defaults->getTitle());
//$tmpl->assign('instance_entity', $defaults->getEntity());
//$tmpl->assign('instance_slogan', $defaults->getSlogan());
$tmpl->assign('isPreviewActive', \OC::$session['extendedtheming-preview-theme']);

return $tmpl->fetchPage();
