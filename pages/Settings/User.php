<?php

/**
* ownCloud
*
* @author Aubrey Hewes
* @copyright 2014 Aubrey Hewes aubrey@hewes.org.uk
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU AFFERO GENERAL PUBLIC LICENSE
* License as published by the Free Software Foundation; either
* version 3 of the License, or any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU AFFERO GENERAL PUBLIC LICENSE for more details.
*
* You should have received a copy of the GNU Affero General Public
* License along with this library.  If not, see <http://www.gnu.org/licenses/>.
*/

if (!OCP\Config::getAppValue('extendedtheming', 'allow_personal_theme', false) === true) {
	return '';
}

OCP\Util::addScript('extendedtheming', 'settings');
OCP\Util::addStyle('extendedtheming', 'settings');

$repository = new \OCA\ExtendedTheming\Repository\Theme();

// hmz.. within this scope the app is not available.. get it..
$app = new \OCA\ExtendedTheming\App('extendedtheming');
$theme = $app->getCurrentThemeSettings();

$tmpl = new OCP\Template('extendedtheming', 'Settings/User');
$tmpl->assign('theme', $theme['name']);
$tmpl->assign('themes', $repository->findAll());
return $tmpl->fetchPage();
