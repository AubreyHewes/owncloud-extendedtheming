<?php

/**
 * Class OC_Theme
 */
class OC_Theme {

	/**
	 * @var OC_L10N
	 */
	private $l;

	/**
	 *
	 */
	public function __construct() {
		$this->l = OC_L10N::get('core');
	}

	/**
	 * Returns the title
	 * @return string title
	 */
	public function getTitle() {
		return \OCP\Config::getAppValue('extendedtheming', 'instance_title', 'ownCloud');
	}

//	/**
//	 * Returns the short name of the software
//	 * @return string title
//	 */
//	public function getName() {
//		return \OCP\Config::getAppValue('extendedtheming', 'instance_name', 'ownCloud');
//	}
//
//	/**
//	 * Returns entity (e.g. company name) - used for footer, copyright
//	 * @return string entity name
//	 */
//	public function getEntity() {
//		return \OCP\Config::getAppValue('extendedtheming', 'instance_entity', 'ownCloud');
//	}
//
//	/**
//	 * Returns slogan
//	 * @return string slogan
//	 */
//	public function getSlogan() {
//		return \OCP\Config::getAppValue('extendedtheming', 'instance_slogan',
//			$this->l->t("web services under your control"));
//	}
//
//	/**
//	 * Returns logo claim
//	 * @return string logo claim
//	 */
//	public function getLogoClaim() {
//		return \OCP\Config::getAppValue('extendedtheming', 'instance_logo_claim', '');
//	}

//	/**
//	 * Returns short version of the footer
//	 * @return string short footer
//	 */
//	public function getShortFooter() {
//		return \OCP\Config::getAppValue('extendedtheming', 'instance_short_footer', 'test1');
//	}

//	/**
//	 * Returns long version of the footer
//	 * @return string long footer
//	 */
//	public function getLongFooter() {
//		return \OCP\Config::getAppValue('extendedtheming', 'instance_long_footer', 'test2');
//	}
}