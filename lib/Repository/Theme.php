<?php
namespace OCA\ExtendedTheming\Repository;

/**
 * Class Theme
 * @package OCA\ExtendedTheming
 */
class Theme {

	/**
	 * @return array
	 */
	public function findAll() {
		static $themes = array();
		if (!empty($themes)) {
			return $themes;
		}
		$themes[] = 'default';
		foreach (glob('themes/*' , GLOB_ONLYDIR) as $theme) {
			$themes[] = preg_replace('#^[^\/]+\/#i', '', $theme);
		}
		return $themes;
	}

	/**
	 * @param string $name
	 * @param string $archive
	 */
	public function add($name, $archive) {
		// 1a: process passed $archive location into themes dir
		// 1b: validate theme (if crap then remove)...
		// 2: add theme to current themes ~ $this->themes[] = $name;
	}

}