<?php
namespace OCA\ExtendedTheming;

/**
 * Class Theme
 * @package OCA\ExtendedTheming
 */
class Theme {

	/**
	 * @var array
	 */
	private $settings;

	/**
	 * @param $settings
	 */
	public function __construct($settings) {
		$this->settings = $settings;
	}

	/**
	 * Apply the theme
	 */
	public function apply() {
		if (!empty($_SERVER['SCRIPT_URL'])
				&& preg_match('#^\/index\.php\/.*\.((c|j)ss?(on)?|png)$|cron\.php$|\/(avatar|ajax)\/#i', $_SERVER['SCRIPT_URL'])) {
			return;
		}

		\OCP\Config::setSystemValue('theme', $this->settings['name']);

		if (!$this->settings['allow_js'] || $this->settings['name'] === 'default') {
			return;
		}

		//////////////////////////////////////////////////
		// if the theme is defined then actively try to load any available "extra" files

		// add the current theme's own javascript files.... {theme}/core/js/*.js
		$dir = 'themes/' . $this->settings['name'] . '/core/js';
		if (!is_dir($dir)) {
			return;
		}
		foreach (scandir('themes/' . $this->settings['name'] . '/core/js') as $file) { // working dir is owncloud home
			if (!preg_match('#(.*)\.js$#i', $file, $matches)) { // only *.js files
				continue;
			}

//			\OCP\Util::writeLog('extendedtheming', 'Adding theme js-file: ' . $matches[1] . ' to: ' . $_SERVER['SCRIPT_URL'], \OCP\Util::DEBUG);

			// add found *.js within {theme}/core/js to the owncloud-ui core js
			\OC_Util::addScript(null, $matches[1]);
		}

	}

}