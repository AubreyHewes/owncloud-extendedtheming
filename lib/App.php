<?php
namespace OCA\ExtendedTheming;

/**
 * Class App
 * @package OCA\ExtendedTheming
 */
class App /* extends \OC\App (and/or) implements \OC\App */ {

	/**
	 * @return array
	 */
	public function getCurrentThemeSettings() {
		return array(
			'name' => $this->getThemeName(),
			'allow_js' => $this->getAllowCustomJs()
		);
	}

	/**
	 * @override
	 */
	public function init()  {
		$theme = new \OCA\ExtendedTheming\Theme($this->getCurrentThemeSettings());
		$theme->apply();
	}

	/**
	 * @override
	 */
	public function getClassMappings() {
		return array(
			'OC_Theme' => 'apps/extendedtheming/lib/OC_Theme.php',
			'OCA\ExtendedTheming\Theme' => 'apps/extendedtheming/lib/Theme.php',
			'OCA\ExtendedTheming\Repository\Theme' => 'apps/extendedtheming/lib/Repository/Theme.php',
		);
	}

	/**
	 * @override
	 * sets the setting screens
	 */
	public function getAdminSettings() {
		return array('pages/Settings/Admin');
	}

	/**
	 * @return array
	 */
	public function getPersonalSettings() {
		if (!\OCP\Config::getAppValue('extendedtheming', 'allow_personal_theme', false) == true) {
			return array();
		}
		return array('pages/Settings/User');
	}

	/**
	 * Helper to get the current theme name
	 * @return string
	 */
	private function getThemeName() {
		static $theme;
		if (!empty($theme)) {
			return $theme;
		}

		$default = \OCP\Config::getAppValue($this->name, 'theme', \OCP\Config::getSystemValue('theme', 'default'));

		// preview or user or default
		$theme = \OC::$session->exists($this->name . '-preview-theme')
			? \OC::$session[$this->name . '-preview-theme']
			: \OCP\Config::getAppValue($this->name, 'allow_personal_theme', false) == true
				? \OCP\Config::getUserValue(\OCP\User::getUser(), $this->name, 'theme', $default)
				: $default;

		return $theme;
	}

	/**
	 * @return string
	 */
	private function getAllowCustomJs() {
		return \OC::$session->exists($this->name . '-preview-allow_custom_theme_js')
			? true
			: \OCP\Config::getAppValue($this->name, 'allow_custom_theme_js', false) == true;
	}

	// @todo using an abstract superclass requires adding it to the classpath; adding the App.php is more than wanted.
	// abstract class/interface

	/**
	 * @var string
	 */
	private $name;

	/**
	 * @var \OCA\AppFramework\Core\API
	 */
	private $api;

	/**
	 * @param string $name
	 */
	public function __construct($name) {
		if(!\OCP\App::isEnabled('appframework')){
			\OCP\Util::writeLog($name,
				'Can not enable the "Extended Theming" app because the "App Framework" app is disabled',
				\OCP\Util::ERROR);
			return;
		}
		$this->name = $name;
		$this->api = new \OCA\AppFramework\Core\API($this->name);
		$this->initClassMappings();
		$this->initAdminSettings();
		$this->initPersonalSettings();
		$this->init();
	}

	/**
	 * sets the required class-mappings
	 * @todo interface?
	 */
	private function initClassMappings() {
		foreach ($this->getClassMappings() as $name => $file) {
			\OC::$CLASSPATH[$name] = $file;
		}
	}

//	/**
//	 * @return array
//	 */
//	public abstract function getClassMappings();

	/**
	 * sets the setting screens
	 * @todo interface?
	 */
	private function initAdminSettings() {
		foreach ($this->getAdminSettings() as $name) {
			\OCP\App::registerAdmin($this->getApi()->getAppName(), $name);
		}
	}

//	/**
//	 * @return array
//	 */
//	public abstract function getAdminSettings();


	/**
	 * sets the setting screens
	 * @todo interface?
	 */
	private function initPersonalSettings() {
		foreach ($this->getPersonalSettings() as $name) {
			\OCP\App::registerPersonal($this->getApi()->getAppName(), $name);
		}
	}

//	/**
//	 * @return array
//	 */
//	public abstract function getPersonalSettings();

//	/**
//	 * @return mixed
//	 */
//	public abstract function initCustom();

	/**
	 * @param \OCA\AppFramework\Core\API $api
	 */
	public function setApi($api) {
		$this->api = $api;
	}

	/**
	 * @return \OCA\AppFramework\Core\API
	 */
	public function getApi() {
		return $this->api;
	}


}