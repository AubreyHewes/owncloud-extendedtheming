# OwnCloud Extended Theme Support

Extends OwnCloud default theme functionality with extra (missing) functionality.

## Description:
This OwnCloud App supports the following:

 * Admin can set the __default__ theme via admin-settings.
 * Admin can preview a theme (session based).
 * Admin can allow themes to have own JavaScript.
 * Admin can let users choose their own theme (theme-selection).
 * User can choose their own theme (if theme-selection is allowed by admin; only active after login).
 * User can preview a theme (if theme-selection is allowed by admin; session based).
 * Admin can set document-title

# Why? (becoming superfluous)
I wanted to "re-colour" the default icons, as the out-of-the-box, owncloud-svg-icons do not really work with a light style.
The icons are SVGs; influencing an SVG contents via CSS is impossible when they are not an inline SVG.
So I needed to convert external svg (/image.svg) to an inline svg (<tags/>).
As then I could influence the svg content via CSS. To do this I needed to load pure custom javascript that
changed the physical remote SVG to an inline SVG. Then I could influence the image.
To do this required loading javascript that has no relation to the rest of the application. This was not possible without
copying a "current" file and appending the functionality; Breaking maintainability.

## Problem
There is not a way to add to or override core javascripts without copying( and appending).
Which may break the system after a core update.

### Requirement
It is a must-have that a theme can contain it's own javascript files.

## FAQ/Comments
But you can just copy the core `X.js` to your theme, and change it according to requirements...

 * After an owncloud update the original `X.js` may have been updated and therefore any new functionality/fixes
will be ignored due to the override.
 * The theme-override functionality for javascript overrides is not implemented.





