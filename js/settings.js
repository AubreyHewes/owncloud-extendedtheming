(function(){
	var OCA = OCA || {};
	OCA.ExtendedTheming = OCA.ExtendedTheming || {
		url: OC.filePath('extendedtheming','ajax','appconfig.php'),
		postCall: function (action, data, callback){
			data.action = action;
			$.post(OCA.ExtendedTheming.url, data, function (result){
				if(result.status === 'success'){
					if(callback){
						callback(result.data);
					}
				}
			},'json');
		},
		getUserValue: function (key, defaultValue, callback) {
			OCA.ExtendedTheming.postCall('getUserValue', {key: key, defaultValue: defaultValue}, callback);
		},
		getUserValues: function (data, callback) {
			OCA.ExtendedTheming.postCall('getUserValues', data, callback);
		},
		setUserValue:  function (key, value, callback) {
			OCA.ExtendedTheming.postCall('setUserValue', {key: key, value: value}, callback);
		},
		setTransientUserValues:  function (data, callback) {
			OCA.ExtendedTheming.postCall('setTransientUserValues', {data: data}, callback);
		},
		setUserValues:  function (data, callback) {
			OCA.ExtendedTheming.postCall('setUserValues', {data: data}, callback);
		},
		getAppValue:  function (key, defaultValue, callback) {
			OCA.ExtendedTheming.postCall('getAppValue', {key: key, defaultValue: defaultValue}, callback);
		},
		getAppValues:  function (data, callback) {
			OCA.ExtendedTheming.postCall('getAppValues', data, callback);
		},
		setAppValue:  function (key, value, callback) {
			OCA.ExtendedTheming.postCall('setAppValue', {key: key, value: value}, callback);
		},
		setAppValues:  function (data, callback) {
			console.log(data);
			OCA.ExtendedTheming.postCall('setAppValues', {data: data}, callback);
		},
		setTransientAppValues:  function (data, callback) {
			OCA.ExtendedTheming.postCall('setTransientAppValues', {data: data}, callback);
		},
		Theme: {
			__apply: function (setMethod, $container) {
				var settings = {};
				$container.find('.form-control').each(function (idx, item) {
					var $item = $(item);
					settings[$item.prop('name')] = $item.is('input[type="checkbox"]')
						? $item.is(':checked') ? 1 : 0
						: $item.val();
				});
				OCA.ExtendedTheming[setMethod](settings, function () {
					window.location = window.location;
				});
			},
			apply: function ($container) {
				OCA.ExtendedTheming.Theme.__apply($container.hasClass('settings-user') ? 'setUserValues' : 'setAppValues', $container);
			},
			preview: function ($container) {
				OCA.ExtendedTheming.Theme.__apply('setTransientAppValues', $container);
			}
		}
	};

	$(document).ready(function() {

		$('#oca-extendedtheming.settings').on('click', '#apply', function(event) {
			OCA.ExtendedTheming.Theme.apply($(event.delegateTarget));

		}).on('click', '#preview', function() {
			OCA.ExtendedTheming.Theme.preview($(event.delegateTarget));
		});
	});

})();