<?php
namespace OCA\ExtendedTheming;

///////////////////////////////////////////////
// create app

// need this for classloading
\OC::$CLASSPATH['OCA\ExtendedTheming\App'] = 'apps/extendedtheming/lib/App.php';

// this does the rest
new App('extendedtheming');
