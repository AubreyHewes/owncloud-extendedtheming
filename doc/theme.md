# How to theme

## Own logo
@TODO(is well-known within the community)

## Override defaults (document-title, footer, etc)
Add a file `defaults.php` to the root of the theme.
This file contents should be:

	class OC_Theme {
	}


### Override the document title
Add a method `getTitle` to the defaults.php (class OC_Theme).

	function getTitle() {
		return 'My Instance Title'; // "ownCloud"
	}

### Override the entity ("ownCloud": company title)
Add a method `getEntity` to the defaults.php (class OC_Theme).

	function getEntity() {
		return 'My Company'; // "ownCloud"
	}


### Override the slogan "web services under your control"
Add a method `getSlogan` to the theme\'s defaults.php (class OC_Theme).

	function getSlogan() {
		return 'My Slogan'; // "web services under your control"
	}


### Override the footer (is based on `$theme->getEntity() + $theme->getSlogan()`)
Add a method `getShortFooter` to the theme\'s defaults.php (class OC_Theme).

	function getShortFooter() {
		return 'My Footer'
	}


### Remove the footer
Add a method `getShortFooter` to the theme\'s defaults.php (class OC_Theme).

	function getShortFooter() {
		return ''; // empty removes the footer
	}

