<?php
/* @var OC_L10N $l */
/* @var array $_ */
?><form id="oca-extendedtheming" class="settings settings-user" role="form">
	<fieldset class="personalblock">
		<h2><?php p($l->t('Theme')); ?></h2>

		<div class="form-group">
			<label class="control-label" for="theme"><?php p($l->t('Personal Theme')); ?></label>
			<div>
				<select class="form-control" name="theme"
					   id="theme">
				<?php foreach ($_['themes'] as $theme): ?>
					<option<?php if ($_['theme'] === $theme) print_unescaped(' selected="selected"');
							?>><?php p($theme); ?></option>
				<?php endforeach; ?>
				</select>
				<span class="help-block"><?php p($l->t('The personal theme is applied after login.')); ?></span>
			</div>
		</div>

		<div class="form-group">
			<label class="control-label"></label>
			<div>
				<input type="button"
					   name="apply"
					   id="apply"
					   value="<?php p($l->t('Apply')); ?>"/>
				<input type="button"
					   name="preview"
					   id="preview"
					   value="<?php p($l->t('Preview')); ?>"/>
				<br/>
				<span class="help-block"><?php p($l->t('Apply or Preview these theme settings')); ?></span>
			</div>
		</div>

	</fieldset>
</form>

