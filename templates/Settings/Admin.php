<?php
/* @var OC_L10N $l */
/* @var array $_ */
?><form id="oca-extendedtheming" class="settings settings-admin" role="form">
	<fieldset class="personalblock">
	<h2><?php p($l->t('Theme')); ?></h2>

		<div class="alert alert-notice">
			<?php print_unescaped($l->t('Have problems/issues with this application? <a target="_blank" href="https://bitbucket.org/AubreyHewes/owncloud-extendedtheming/">add them here</a>!')); ?>
		</div>

		<?php if (isset($_['dependencies']) and ($_['dependencies']<>'')) print_unescaped(''.$_['dependencies'].''); ?>

		<div class="form-group">
			<label class="control-label" for="theme"><?php p($l->t('Instance Theme')); ?></label>
			<div>
				<select class="form-control" name="theme"
					   id="theme">
				<?php foreach ($_['themes'] as $theme): ?>
					<option<?php if ($_['theme'] === $theme) print_unescaped(' selected="selected"');
							?>><?php p($theme); ?></option>
				<?php endforeach; ?>
				</select>
				<span class="help-block"><?php p($l->t('The default theme of this instance')); ?></span>
			</div>
		</div>

		<div class="form-group">
			<label class="control-label" for="allow_custom_theme_js"><?php p($l->t('Enable Theme JavaScripts')); ?></label>
			<div>
				<input class="form-control" type="checkbox" name="allow_custom_theme_js" id="allow_custom_theme_js"
					<?php if ($_['allow_custom_theme_js']) print_unescaped(' checked="checked"'); ?> />
				<span class="help-block"><?php p($l->t('Allow themes to add own javascript. All js files within the theme\'s js folder are added to the generated javascript.')); ?></span>
			</div>
		</div>

		<div class="form-group">
			<label class="control-label" for="allow_personal_theme"><?php p($l->t('Enable User Theming')); ?></label>
			<div>
				<input class="form-control" type="checkbox" name="allow_personal_theme" id="allow_personal_theme"
					<?php if ($_['allow_personal_theme']) print_unescaped(' checked="checked"'); ?> />
				<span class="help-block"><?php p($l->t('Allow users to select their own theme (from available themes). Is applied after login')); ?></span>
			</div>
		</div>

		<div class="alert alert-warning">
			<?php p($l->t('The following settings will only be applied if the theme does not contain it\'s own defaults.')); ?>
		</div>

		<div class="form-group">
			<label class="control-label" for="instance_title"><?php p($l->t('Instance Title')); ?></label>
			<div>
				<input class="form-control" type="text" name="instance_title" id="instance_title"
					value="<?php if ($_['instance_title']) p($_['instance_title']); ?>" />
				<span class="help-block"><?php p($l->t('The title of this instance (i.e. the page title)')); ?></span>
			</div>
		</div>

		<?php /*
		<div class="form-group">
			<label class="control-label" for="instance_entity"><?php p($l->t('Instance Entity')); ?></label>
			<div>
				<input class="form-control" type="text" name="instance_entity" id="instance_entity"
					value="<?php if ($_['instance_entity']) p($_['instance_entity']); ?>" />
				<span class="help-block"><?php p($l->t('The entity of this instance (i.e. the company)')); ?></span>
			</div>
		</div>

		<div class="form-group">
			<label class="control-label" for="instance_slogan"><?php p($l->t('Instance Slogan')); ?></label>
			<div>
				<input class="form-control" type="text" name="instance_slogan" id="instance_slogan"
					value="<?php if ($_['instance_slogan']) p($_['instance_slogan']); ?>" />
				<span class="help-block"><?php p($l->t('The slogan of this instance (i.e. the slogan)')); ?></span>
			</div>
		</div>
		 */ ?>

		<div class="form-group">
			<label class="control-label"></label>
			<div>
				<input type="button"
					   name="apply"
					   id="apply"
					   value="<?php p($l->t('Apply')); ?>"/>
				<input type="button"
					   name="preview"
					   id="preview"
					   value="<?php p($l->t('Preview')); ?>"/>
				<br/>
				<span class="help-block"><?php p($l->t('Apply or Preview these theme settings')); ?></span>
			</div>
		</div>

	</fieldset>
</form>

