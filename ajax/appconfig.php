<?php
/* @var array $param */

OCP\JSON::checkLoggedIn();
OCP\JSON::checkAppEnabled($param['app']);
OCP\JSON::callCheck();

$user = OCP\User::getUser();

$action=isset($_POST['action'])?$_POST['action']:$_GET['action'];

if (strstr($action, 'App')) {
	OCP\JSON::checkAdminUser();
}

$result=false;
switch($action) {

	case 'getUserValue':
		$result = OCP\Config::getUserValue($user, $param['app'], $_GET['key'], $_GET['defaultValue']);
		break;
	case 'getUserValues':
		$result = array();
		foreach ($_POST['data'] as $key => $defaultValue) {
			$result[$key] = OCP\Config::getUserValue($user, $param['app'], $key, $defaultValue);
		}
		break;
	case 'setUserValue':
		$result = OCP\Config::setUserValue($user, $param['app'], $_POST['key'], $_POST['value']);
		break;
	case 'setUserValues':
		$result = array();
		foreach ($_POST['data'] as $key => $value) {
			$result[$key] = OCP\Config::setUserValue($user, $param['app'], $key, $value);
		}
		break;

	case 'getAppValue':
		$result=OC_Appconfig::getValue($param['app'], $_GET['key'], $_GET['defaultValue']);
		break;
	case 'getAppValues':
		$result = array();
		foreach ($_POST['data'] as $key => $defaultValue) {
			$result[$key] = OCP\Config::getAppValue($param['app'], $key, $defaultValue);
		}
		break;
	case 'setAppValue':
		$result = OCP\Config::setAppValue($param['app'], $_POST['key'], $_POST['value']);
		break;
	case 'setAppValues':
		$result = array();
		foreach ($_POST['data'] as $key => $value) {
			unset(\OC::$session[$param['app'] . '-preview-' . $key]);
			$result[$key] = OCP\Config::setAppValue($param['app'], $key, $value);
		}
		break;

	case 'setTransientValues':
		$result = array();
		foreach ($_POST['data'] as $key => $value) {
			\OC::$session[$param['app'] . '-preview-' . $key] = $value;
		}
		break;
	case 'clearTransientValues':
		$result = array();
		foreach ($_POST['data'] as $key => $value) {
			unset(\OC::$session[$param['app'] . '-preview-' . $key]);
		}
		break;
}
OC_JSON::success(array('data'=>$result));
